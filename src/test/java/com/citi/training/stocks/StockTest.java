package com.citi.training.stocks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class StockTest {
	long stockId = 99;
	String testTicker = "Bob";
	String testCompanyName = "Building Bobs";

	@Test
	public void test_Stock_testFullConstructor() {
		Stock testEmp = new Stock(stockId, testTicker, testCompanyName);

		assertEquals(stockId, testEmp.getId());
		assertEquals(testTicker, testEmp.getTicker());
		assertEquals(testCompanyName, testEmp.getCompanyName());
	}



	@Test
	public void test_Stock_toString() {
		Stock testEmp = new Stock(stockId, testTicker, testCompanyName);

		assertTrue("toString() should contain id", testEmp.toString().contains(Long.toString(stockId)));
		assertTrue("toString() should contain name", testEmp.toString().contains(testTicker));
		assertTrue("toString() should contain salary", testEmp.toString().contains(testCompanyName));
	}
}
