package com.citi.training.stocks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stock")
public class StockController {

	@Autowired
	StockDao stockDao;

	private static final Logger LOG = LoggerFactory.getLogger(StockController.class);

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Stock> findAll() {
		LOG.info("HTTP GET to FINDALL()");
		return stockDao.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Stock findById(@PathVariable long id) {
		LOG.info("HTTP GET  to FINDBYID()");
		return stockDao.findById(id).get();
	}

	@RequestMapping(method = RequestMethod.POST)
	public void create(@RequestBody Stock Stock) {
		LOG.info("HTTP POST  to CREATESTOCK()");
		stockDao.save(Stock);
	}

	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteById(@PathVariable long id) {
		LOG.info("HTTP DELETE  to DELETEBYID()");
		stockDao.deleteById(id);
	}

}
