CREATE DATABASE stockdb;

use stockdb;

SET FOREIGN_KEY_CHECKS=0;


DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock`(
	`stockId` INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`ticker` VARCHAR(30) NOT NULL,
	`companyName` VARCHAR(50) NOT NULL,
)ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Populate table
INSERT INTO `stock` VALUES(1, "YAH", "Yahoo");
INSERT INTO `stock` VALUES(2, "APPL", "Apple");
INSERT INTO `stock` VALUES(3, "BT", "BT Telecoms");
